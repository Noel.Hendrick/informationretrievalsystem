import pandas as pd
import re
import math
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
sw_nltk = stopwords.words('english')

def GetAllDocuments(filepath):
    with open(filepath, "r") as myfile:
        data = myfile.read()
        return data

def DataPreprocessing(text):
    text = text.lower()
    cleantext = re.sub('\W+', ' ', text)
    words = [word for word in cleantext.split(' ') if word.lower() not in sw_nltk]
    words = [x for x in words if not any(c.isdigit() for c in x)]
    new_text = " ".join(words)
    return new_text

def MakeDataframe(filepath):
    data = GetAllDocuments(filepath)
    df = pd.DataFrame(columns=['ID', 'Title', 'Authors', 'Text'])
    while len(data) > 5:
        _id = data[3:data.index('.T')].strip()
        data = data[data.index('.T'):]
        _title = data[3:data.index('.A')].strip()
        data = data[data.index('.A'):]
        _author = data[3:data.index('.B')].strip()
        data = data[data.index('.W'):]
        _text = data[3:data.index('.I')].strip()
        _text = DataPreprocessing(_text)
        data = data[data.index('.I'):]
        row = [[_id, _title, _author, _text]]
        df2 = pd.DataFrame(row, columns=['ID','Title','Authors','Text'])
        df = df.append(df2, ignore_index=True)
    return(df)

def MakeQueryDataframe(filepath):
    data = GetAllDocuments(filepath)
    df = pd.DataFrame(columns=['ID','Text'])
    while len(data) > 5:
        _id = data[3:data.index('.W')].strip()
        data = data[data.index('.W'):]
        _text = data[3:data.index('.I')].strip()
        _text = DataPreprocessing(_text)
        data = data[data.index('.I'):]
        row = [[_id,_text]]
        df2 = pd.DataFrame(row, columns=['ID', 'Text'])
        df = df.append(df2, ignore_index=True)
    return (df)



def MakeWordDictionary(df):
    UniqueWords = set()
    for index, document in df.iterrows():
        unique = set(document['Text'].split(' '))
        UniqueWords = UniqueWords.union(unique)
    UniqueWords.remove("")
    return UniqueWords

def findNumberOfOccurances(word, text, total):
    num = text.split().count(word)

    num = num * math.log(1400/total)
    return num

def NumberOfDocuments(word, df):
    x = 0
    for index, document in df.iterrows():
        if word in document['Text']:
            x = x+1
    if x == 0:
        return 1
    else:
        return x

def NormaliseVector(row, magnitude):
    for key in row:
        row[key] = row[key]/magnitude
    return row


def MakeBitMaps(df,dict):
    bitmaps = {}
    for index, document in df.iterrows():
        row = {}
        magnitude =0
        for word in dict:
            total = NumberOfDocuments(word ,df)
            number = findNumberOfOccurances(word, document['Text'], total)
            magnitude = magnitude + (number*number)
            row[word] = number
        # remeber that the bitmap will index at 0 but id's will index at 1
        magnitude = math.sqrt(magnitude)
        row = NormaliseVector(row, magnitude)
        bitmaps[document['ID']] = row
    return bitmaps

def RankDocuments(queryBitmap, bitmaps):
    DocumentRanks = {}
    for key in bitmaps:
        bitmap = bitmaps[key]
        x = 0
        for word in queryBitmap:
            x = x + bitmap[word] * queryBitmap[word]
        DocumentRanks[key] = x
    return sorted(DocumentRanks.items(), key=lambda x: x[1], reverse=True)

def Evaluation(rank, id):
    i = 0
    for key in rank:
        s = id.lstrip('0') +' Q0 '+key[0]+ ' 0 '+ str(key[1]) +' 0'
        # Open a file with access mode 'a'
        file_object = open('Results.txt', 'a')
        file_object.write(s)
        file_object.write("\n")
        file_object.close()
        if 1 > 100:
            break
        i = i+1


def RunQuerys(queryDF, dict, bitmaps):
    for index, query in queryDF.iterrows():
        data = [[query['ID'],query['Text']]]
        q = pd.DataFrame(data,columns=['ID','Text'])
        queryBitmap = MakeBitMaps(q, dict)
        Rank = RankDocuments(list(queryBitmap.items())[0][1], bitmaps)
        print(Rank)
        Evaluation(Rank, query['ID'])
    print("All queries completed")



def main():
    df = MakeDataframe("cran-abstracts-1400.txt")
    dict = MakeWordDictionary(df)
    bitmaps = MakeBitMaps(df,dict)
    queryDF =  MakeQueryDataframe("cran-queries-updated.txt")
    RunQuerys(queryDF,dict,bitmaps)

main()